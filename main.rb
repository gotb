#!/usr/bin/ruby18 -w

require 'Qt4'
require 'BoardMap'
require 'Track'

Qt::Application.new(ARGV) do
#	Qt.debug_level = Qt::DebugLevel::High
	Qt::Widget.new do
		self.window_title = 'Game Of Thrones: Board Game 2nd Edition'

		resize(640,480)

		btnQuit = Qt::PushButton.new('Quit') do
			connect(SIGNAL :clicked) { Qt::Application.instance.quit }
			setStyleSheet('background-color: #ff0000;');
		end

		#label = Qt::Label.new('OUT')

		map = GOTB::BoardMap.new
	#	map.resize(300,100)
	#	map.show
		#map = GOTB::BoardMap.new
		
		tracks = GOTB::Tracks.new
		#tracks.tracks[0].show
		tracks.show
		
		self.layout = Qt::VBoxLayout.new do
			#add_widget(tracks.tracks[0].view);
			add_widget(btnQuit, 0, Qt::AlignCenter)
			add_widget(map)
		end

		show
	end

	exec
end
