module GOTB

module House
	EMPTY 		= -1
	BARATHEON = 0
	LANISTER 	= 1
	STARK 		= 2
	GREYJOY 	= 3
	TYRELL		= 4
	MARTELL		= 5
end

HOUSE_DATA = [
	{:token => 'data/token_baratheon.png'},
	{:token => 'data/token_lannister.png'},
	{:token => 'data/token_stark.png'},
	{:token => 'data/token_greyjoy.png'},
	{:token => 'data/token_tyrell.png'},
	{:token => 'data/token_martell.png'}
]

class Tracks < Qt::Widget
	def initialize
		super

		@tracks = []

		ll = Qt::VBoxLayout.new

		1.times {|i|
			@tracks << Track.new(i)

			btn = Qt::PushButton.new("ready #{i}")
			btn.setSizePolicy(Qt::SizePolicy::Minimum, Qt::SizePolicy::Expanding)
			btn.connect(:clicked, @tracks.last, :updateOrder)

			l = Qt::HBoxLayout.new
			l.addWidget(@tracks.last)
			l.addWidget(btn)

			w = Qt::Widget.new
			w.layout = l
			
			ll.addWidget(w)
		}

		self.layout = ll
	end
end

class Track < Qt::GraphicsView
	THRONE 	= 0
	FIEFDOM	= 1
	COURT		= 2

	attr_accessor :scene,:order,:tokens,:view,:type

	slots :updateOrder

	TRACK_W = 1150
	TRACK_H = 146

	TRACK_W_2 = 624
	TRACK_H_2 = 133

	TOKEN_W = 100
	TOKEN_H = 95

	def initialize(kind, players = 6, parent = 0)
		#super(parent)
		super()

		@kind = kind
		@players = players

		pix = case @kind
			when THRONE then Qt::Pixmap.new("data/track_throne.png")
			when FIEFDOM then Qt::Pixmap.new("data/track_fiefdom.png")
			when COURT then Qt::Pixmap.new("data/track_court.png")
			else puts "Track: invalid type #{type}"
		end
		#pix = Qt::Pixmap.new("data/map_no_tracks_no_wild.jpg")
		#pix = Qt::Pixmap.new("data/map_no_tracks.jpg")
		#bg = Qt::GraphicsPixmapItem.new(pix)
		@bg = Over.new(pix)
		#@bg.graphicsEffect = Qt::GraphicsBlurEffect.new
		@bg.graphicsEffect = Qt::GraphicsOpacityEffect.new
		@bg.graphicsEffect.opacity = 0.4

		@tokens = []
		@players.times {|i|
#			@order[i] = House::BARATHEON + i;
			@tokens << Token.new(House::BARATHEON + i, @bg)
			#@tokens.last.setFlags(Qt::GraphicsItem::ItemIsMovable | Qt::GraphicsItem::ItemSendsGeometryChanges)
			#@tokens.last.setPos(40+152*i,15)
			@tokens.last.setPos(240+150*i, 22)
		}

		@scene = Qt::GraphicsScene.new
		@scene.setSceneRect(0,0,TRACK_W,TRACK_H)
		#@scene.setSceneRect(0,0,pix.width,pix.height)
		@scene.addItem(@bg)

		self.setScene(@scene)

		self.verticalScrollBarPolicy = Qt::ScrollBarAlwaysOff
		self.horizontalScrollBarPolicy = Qt::ScrollBarAlwaysOff

		@high = true
		self.setInteractive(@high)
	end

	def updateOrder()
		puts "updateOrder"
		@players.times {|i|
			cx = 240 + 150 * i
			cy = 22
			ni = Hash[@tokens.each_with_index.map{|t,j| [j, Math.hypot(cx - t.x, cy - t.y)]}].min_by{|k,v| v}[0]
			@tokens[i],@tokens[ni] = @tokens[ni],@tokens[i]
			@tokens[i].x = cx
			@tokens[i].y = cy
		}

		@high = !@high
		setInteractive(@high)

		puts "new order: #{@tokens.map {|t| t.house}.join(',')}"
	end

	def setInteractive(enabled)
		@bg.graphicsEffect.enabled = !enabled
		@bg.enabled = enabled
	end

	def resizeEvent(event)
		self.fitInView(@bg)
	end
end

class Token < Qt::GraphicsPixmapItem
	attr_reader :house

	def initialize(house, p = 0)
		@house = house
		pix = Qt::Pixmap.new(HOUSE_DATA[@house][:token])

		super(pix, p)

		@finished = false

		self.setFlags(Qt::GraphicsItem::ItemIsMovable | Qt::GraphicsItem::ItemSendsGeometryChanges)
	end

	def mousePressEvent(event)
		self.zValue = 1

		super(event)
	end

	def mouseReleaseEvent(event)
		@finished = false if (@finished)
		self.zValue = 0

		super(event)
	end

	def itemChange(change, value)
		@finished = true if (change == Qt::GraphicsItem::ItemPositionHasChanged)
		
		return super(change, value)
	end
end

class Over < Qt::GraphicsPixmapItem
end

class TrackStars < Track
	STARS_A = [3,2,1,0,0,0]
	STARS_B = [3,3,2,1,0,0]

	def initialize
		super

		@stars = STARS_A
	end

	def setStars(ppl)
		if (ppl >= 5)
			@stars = STARS_B
		else 
			@stars = STARS_A
		end
	end
end

end
