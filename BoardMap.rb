module GOTB

require 'Qt4'
require 'json'

#MAP_W = 1980
#MAP_H = 2975

class BoardMap < Qt::Widget
	attr_accessor :view
	def initialize
		super

		w = 400
		h = 200

		@regions = Array.new
		#regs_in = JSON.parse(File.open('data/regions.json').read))
		#regs_in.each {|name,data|
		#	@regions << Qt::GraphicsPolygonItem.new(Qt::PolygonF.new(data.map{|pt| Qt::Point.new(pt[0],pt[1])}))
		#}

		#points = [10,10, 20,13, 24,55, 8,50, 10,10]
		points = [10,10, 20,13, 24,55, 8,50]
		poly = Array.new
		points.each_slice(2) {|s|
			poly << Qt::Point.new(s[0],s[1])
			puts "x: #{s[0]}, y: #{s[1]}"
		}
		#@regions << BoardRegion.new(Qt::PolygonF.new(poly))
		@regions << Qt::GraphicsPolygonItem.new
		@regions.last.setPolygon(Qt::PolygonF.new(poly))

#		pp = Qt::PolygonF.new(poly)
#		puts "size = #{@regions.size}"

#		p = @regions[0].polygon
#		puts("poly = #{p.first()}")
#		[10, 20, 50].each {|x|
#			reg = BoardRegion.new
#			reg.setPos(x,x)
#			@regions << reg
#		}

		pix = Qt::Pixmap.new("data/map.jpg")
		#pix = Qt::Pixmap.new("data/map_no_tracks_no_wild.jpg")
		#pix = Qt::Pixmap.new("data/map_no_tracks.jpg")
		map = Qt::GraphicsPixmapItem.new(pix)

		@scene = Qt::GraphicsScene.new
		@scene.setSceneRect(0,0,pix.width,pix.height)
		@regions.each {|r| @scene.addItem(r)}
		@scene.addItem(map)

		@view = Qt::GraphicsView.new
		#@view.resize(w,h)
		@view.setScene(@scene)
		#@view.setAttribute(Qt::WA_TransparentForMouseEvents, true)
		
		setMouseTracking(true)

		grid = Qt::GridLayout.new
		grid.setContentsMargins(0,0,0,0)
		grid.setSpacing(0)
		grid.addWidget(@view, 0, 0)
		self.layout = grid
	end

#def mouseMoveEvent(event)
#		puts "mouse = #{event.x},#{event.y}\n"
#		super(event)
#	end
end

class BoardRegion < Qt::GraphicsPolygonItem
end

end
